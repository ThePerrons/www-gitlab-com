---
layout: markdown_page
title: "Product Career Development Framework Working Group"
canonical_path: "/company/team/structure/working-groups/product-career-development-framework/"
description: "Learn more about the Product Career Development Framework Working Group goals, processes, and teammates!"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Attributes

| Property        | Value           |
|-----------------|-----------------|
| Date Created    | April 21, 2021 |
| Target End Date | September 30, 2021 |
| Slack           | [#wg_product-cdf](https://join.slack.com/share/zt-pg3dw504-aLrkdftFZ9xrqHEGqgqQMw) (only accessible by GitLab team members) |
| Google Doc      | [Working Group Agenda](https://docs.google.com/document/d/1a0baXkNsfDuDwcJ5IxNlLekCmFzElMGnmTmVAfQYV8o/edit?usp=sharing) (only accessible by GitLab team members) |
| Docs            | TBD |
| Issues    | [Issue List](https://gitlab.com/gitlab-com/Product/-/issues?label_name[]=wg%3Aproduct-cdf) / [Issue Board](https://gitlab.com/gitlab-com/Product/-/boards/2778676?&label_name[]=wg%3Aproduct-cdf) |
| Label           | `~wg-product-cdf` |
| Associated KPIs/OKRs | TBD |
| GitLab group for working group| `@wg-product-cdf |


## Business Goals

1. Deliver a revised [Product Career Development Framework](https://about.gitlab.com/handbook/product/product-manager-role/product-CDF-competencies/#product-management-career-development-framework) for Individual Contributor roles to Group Manager of Product based on new competencies by 2021-09-30. 
1. Conduct AMAs and training with Product Organization on new CDFs for use in FY22-Q4 Career Conversations. 

### Protocols and Processes

1. We will have a recurring weekly sync with the working group 
1. We will use a issue board for assigning tasks and delegating efforts across the working group 
1. We will track efforts in phases, which will be broken up into epics 
1. The DRI for the CDF is the VP of Product Management and all changes for the CDF will be approved via the VP

### Exit Criteria

1. Research and incorporation of industry standards into Product Manager [Skills](/handbook/product/product-manager-role/product-CDF-competencies/#cdf-for-individual-contributors) and [Requirements](/job-families/product/product-manager/#base-requirements-across-all-levels) => 100% delivered in [Product#2460](https://gitlab.com/gitlab-com/Product/-/issues/2460#note_632540218)
1. Research and incorporation of internal expectations into Product Management [Responsibilities](https://about.gitlab.com/handbook/product/product-manager-responsibilities/) => 100% delivered in [Product#2517](https://gitlab.com/gitlab-com/Product/-/issues/2517) and [Product#2832](https://gitlab.com/gitlab-com/Product/-/issues/2832)
1. Clear quantative measures for progress [included in the CDF](/handbook/product/product-manager-role/product-CDF-competencies/#cdf-topics-for-product-managers) 
1. [Training](/handbook/product/product-manager-role/product-CDF-competencies/#people-management-competencies) for how to use the CDF for all [Product Leaders ](/handbook/product/product-leadership/#product-leaders)
1. Simplification of [PM Competencies](/handbook/product/product-manager-role/product-CDF-competencies/)


## Roles and Responsibilities

| Working Group Role    | Person                | Title                          |
|-----------------------|-----------------------|--------------------------------|
| Executive Sponsor     | Anoop Dawar | VP, Product Management |
| Facilitator           | Jackie Porter | Group Manager, Product |
| Contributor           | Farnoosh Seifoddini | Principal PM, Product Operations|
| Contributor           | Kenny Johnston  |  Senior Director of Product, Ops  |
| Functional Lead       | Hila Qu |  Director of Product, Growth |
| Functional Lead       | Joshua Lambert | Director of Product, Enablement |
| Functional Lead       | David DeSanto  | Senior Director of Product, Dev & Sec |
| Reviewer              | Gabe Weaver | Senior Product Manager |
| Reviewer              | Sam Kerr | Principal PM|
| Reviewer              | Michael Karampalas | Principal PM |
| Reviewer              | Taylor McCaslin | Principal PM, Secure|
| Reviewer              | Christen Dybenko | Senior Product Manager|
| Reviewer              | Viktor Nagy | Senior Product Manager|
| Reviewer              | Keith Chung | Senior Director, Pricing |

## Meetings

Meetings are recorded and available on
YouTube in the [Working Group - Product CDF](https://www.youtube.com/playlist?list=PL05JrBw4t0KrBcM2ew6AA00KC9J-PpZ1X) playlist. The playlist is public, although, some meetings maybe private due to the nature of the material covered in the call. 

