---
layout: handbook-page-toc
title: Growth:Adoption Group
description: "The Growth:Adoption group works on feature enhancements and growth experiments across GitLab projects"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Vision

The Adoption Group is part of the [Growth Sub-department]. We work on [connecting our users with our product value](/direction/growth/). 

* I have a question. Who do I ask?

Questions should start by @ mentioning the Product Manager for the [Adoption group](/handbook/product/categories/#adoption-group)
or creating a new issue in the Growth Product [Adoption issues] list.

## Mission

The Growth:Adoption Group works on the Adopt Create [KPI](/handbook/product/performance-indicators/#new-group-namespace-create-stage-adoption-rate)
and contributes to Growth initiatives including [First Mile](https://gitlab.com/groups/gitlab-org/-/epics/4745) (New user flow)
and [Continuous onboarding](https://gitlab.com/groups/gitlab-org/-/epics/4817).

## Team Members

The following people are permanent members of the Growth:Adoption team:

<%= direct_team(manager_role: 'Fullstack Engineering Manager, Growth:Activation, Adoption, Conversion and Expansion', role_regexp: /(Growth:Adoption)/) %>

### Group members

We work directly with the following team members in the Growth:Adoption group:

<%=
other_manager_roles = ['Director of Engineering, Growth, Fulfillment, and Applied ML']
direct_managers_role = 'Fullstack Engineering Manager, Growth:Activation, Adoption, Conversion and Expansion'
roles_regexp = /Adoption/

stable_counterparts(role_regexp: roles_regexp, direct_manager_role: direct_managers_role, other_manager_roles: other_manager_roles)
%>

## Functional Counterparts

We collaborate with our colleagues in the Growth Sub-department teams:

* [Activation](/handbook/engineering/development/growth/activation/)
<!-- * [Adoption](/handbook/engineering/development/growth/adoption/) -->
* [Conversion](/handbook/engineering/development/growth/conversion/)
* [Expansion](/handbook/engineering/development/growth/expansion/)
* [Product Intelligence](/handbook/engineering/development/growth/product-intelligence/)

As well as the wider Growth Sub-department [stable counterparts](/handbook/engineering/development/growth/#stable-counterparts).

### UX
The Growth UX team has a [handbook page](/handbook/product/growth/) which includes [Growth specific workflows](/handbook/product/growth/#how-we-work).

## How we work

* We're data savvy
* In accordance with our [GitLab values]
* Transparently: nearly everything is public
* We get a chance to work on the things we want to work on
* Everyone can contribute; no silos

### Prioritization

Prioritization is a collaboration between Product, UX, Data, and Engineering.

* We use the [ICE framework](/direction/growth/#growth-ideation-and-prioritization) for experiments.
* We use [Priority](https://docs.gitlab.com/ee/development/contributing/issue_workflow.html#priority-labels)
  and [Severity](https://docs.gitlab.com/ee/development/contributing/issue_workflow.html#severity-labels) labels for bugs.

### Workflow

We use the [Product Development workflow](/handbook/product-development-flow/) when working on issues and
merge requests across multiple projects.

While not all of the work we do relates to the GitLab project directly, we use
[milestones](https://docs.gitlab.com/ee/user/project/milestones/) to track `Deliverables` and other enhancements.

#### Overview

| Board | Description |
| ------ | ------ |
| [Planning] |  This board shows Adoption team work that has been allocated to a particular milestone. |
| [Deliverables] | A subset of the milestone board shows issues the Product Manager has determined to be `Deliverables`. |

#### Working boards

* The validation track is where the Product Manager - usually in collaboration with `UX` - defines what the team should aim to deliver. Once complete, the Product Manager will move to `workflow::planning breakdown` (larger issues) or straight to `workflow::scheduling` for Engineering to pick up. If there is no Engineering input required the issue can be closed.
* The Adoption Engineering group (`group::adoption`) schedules issues for development in the build phase, based on the Product Managers priorities. For `bugs` and `security issues`, [Priority](https://docs.gitlab.com/ee/development/contributing/issue_workflow.html#priority-labels) and Severity labels will have been added by the Product Manager.
* Combined workflow board shows the combined workflow including validation and build tracks.

| Board | GitLab.org | GitLabServices |
| ----- | ---        | ---            |
| Validation (PM/UX) | [All](https://gitlab.com/groups/gitlab-org/-/boards/1506660?scope=all&utf8=%E2%9C%93&state=opened&label_name%5B%5D=devops%3A%3Agrowth&label_name%5B%5D=group%3A%3Aadoption) | [All](https://gitlab.com/groups/gitlab-services/-/boards/1687029?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Aadoption) |
| Build (Eng) | [All](https://gitlab.com/groups/gitlab-org/-/boards/1506701?scope=all&utf8=%E2%9C%93&state=opened&label_name%5B%5D=devops%3A%3Agrowth&label_name%5B%5D=group%3A%3Aadoption) | [All](https://gitlab.com/groups/gitlab-services/-/boards/1436668?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Aadoption) |
| Combined Workflow | [All](https://gitlab.com/groups/gitlab-org/-/boards/1158847?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Aadoption) | [All](https://gitlab.com/groups/gitlab-services/-/boards/1546865?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Aadoption) |

The above boards can be filtered by milestone to provide additional context around priority.
For example a `priority::3` security issue ([due within 90 days](/handbook/engineering/security/#severity-and-priority-labels-on-security-issues))
will be added to one of the next 3 milestones by the Engineering team during the `workflow::scheduling` stage to ensure that SLA is met.

## Common Links

* `#g_adoption` in [Slack](https://gitlab.slack.com/archives/g_adoption) (GitLab internal)
* [Adoption issues]
* [Growth Sub-department]
* [Growth issues board]
* `#s_growth` in [Slack](https://gitlab.slack.com/archives/s_growth) (GitLab internal)
* [Growth engineering metrics]
* [Growth opportunities]
* [Growth meetings and agendas]
* [GitLab values]

[Adoption issues]: https://gitlab.com/gitlab-org/growth/product/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=group%3A%3Aadoption

[Growth sub-department]: /handbook/engineering/development/growth/
[Growth issues board]: https://gitlab.com/groups/gitlab-org/-/boards/1158847
[Growth engineering metrics]: /handbook/engineering/metrics/growth/
[Growth opportunities]: https://gitlab.com/gitlab-org/growth/product
[Growth meetings and agendas]: https://drive.google.com/drive/search?q=type:document%20title:%22Growth%20Weekly%22
[GitLab values]: /handbook/values/

[Planning]: https://gitlab.com/groups/gitlab-org/-/boards/1370834?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Aadoption
[Deliverables]: https://gitlab.com/groups/gitlab-org/-/boards/1370834?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Aadoption&label_name[]=Deliverable
[Growth:Adoption Validation track]: https://gitlab.com/groups/gitlab-org/-/boards/1506660?scope=all&utf8=%E2%9C%93&state=opened&label_name%5B%5D=devops%3A%3Agrowth&label_name%5B%5D=group%3A%3Aadoption
[Growth:Adoption Build track]: https://gitlab.com/groups/gitlab-org/-/boards/1506701?scope=all&utf8=%E2%9C%93&state=opened&label_name%5B%5D=devops%3A%3Agrowth&label_name%5B%5D=group%3A%3Aadoption
[Growth:Adoption Workflow]: https://gitlab.com/groups/gitlab-org/-/boards/1158847?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Aadoption
