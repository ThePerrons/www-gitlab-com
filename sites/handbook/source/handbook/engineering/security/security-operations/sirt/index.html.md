---
layout: handbook-page-toc
title: Security Incident Response Team - SIRT
description: GitLab Security Incident Response Team Overview 
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

----

The Security Incident Response Team - SIRT is on the forefront of security events that impact both GitLab.com and GitLab the company.

## <i class="fas fa-rocket" id="biz-tech-icons"></i> Our Vision

To detect security incidents before they happen and to respond promptly when they do happen. 

### Our Mission Statement

Ensure maximum operational uptime of mission critical infrastructure and informational assets in its daily operations. This mission is achieved by providing effective crisis response, timely distribution of security notifications, continuous monitoring of potential issues, postmortem of major incidents for training and environmental awareness.

## <i class="fas fa-users" id="biz-tech-icons"></i> The Team

### Team Members

The following people are permanent members of the SIRT

<table>
<thead>
<tr>
<th>Person</th>
<th>Role</th>
</tr>
</thead>
<tbody>
<tr>
<td>Bistra Lutz</td>
<td><a href="/job-families/engineering/security-incident-response-team/#manager-security-incident-response-team">Manager, SIRT</a></td>
</tr>
<tr>
<td>Mitra Jozenazemian</td>
<td><a href="/job-families/engineering/security-incident-response-team/#senior-security-incident-response-team-engineer">Senior Security Engineer, SIRT</a></td>
</tr>
<tr>
<td>Corey McCarty</td>
<td><a href="/job-families/engineering/security-incident-response-team/#senior-security-incident-response-team-engineer">Senior Security Engineer, SIRT</a></td>
</tr>
<tr>
<td>Harjeet Sharma</td>
<td><a href="/job-families/engineering/security-incident-response-team/#senior-security-incident-response-team-engineer">Senior Security Engineer, SIRT</a></td>
</tr>
<tr>
<td>Aaron Blanco</td>
<td><a href="/job-families/engineering/security-incident-response-team/#senior-security-incident-response-team-engineer">Senior Security Engineer, SIRT</a></td>
</tr>
<tr>
<td>Bala Allam</td>
<td><a href="/job-families/engineering/security-incident-response-team/#senior-security-incident-response-team-engineer">Security Engineer, SIRT</a></td>
</tr>
<tr>
<td>Valentine Mairet</td>
<td><a href="/job-families/engineering/security-incident-response-team/#senior-security-incident-response-team-engineer">Senior Security Engineer, SIRT</a></td>
</tr>
<tr>
<td>Tuan Lam</td>
<td><a href="/job-families/engineering/security-incident-response-team/#senior-security-incident-response-team-engineer">Senior Security Engineer, SIRT</a></td>
</tr>
</tbody>
</table>

## <i class="fas fa-stream" id="biz-tech-icons"></i> Services We Provide:
1. Reactive - Services design to respond to active incident handling, including but not limited to 
- Incident analysis 
- Incident response support and coordination 
- Incident response resolution 
2. Proactive - Services designed to improve the infrastructure  and security  processes of Gitlab before any incident occurs or is detected. The main goals are to avoid incidents and to reduce the impact and scope when they do occur. 
- Cyber Threat Analysis of vulnerability warnings and security advisories
- Monitor Adversaries' activities and related trends to help identify future threats 
- Configuration and maintenance of security tools, applications, and infrastructure
4. Administrative - Services design to assist with requests from Gitlab's Legal and HR Departments.

## <i class="fas fa-bullseye" id="biz-tech-icons"></i> Engaging SIRT

If an urgent security incident has been identified or you suspect an incident may have occurred, please refer to [Engaging the Security On-Call](/handbook/engineering/security/#engaging-the-security-on-call).

### Security Responder On-Call

The [SIRT](/handbook/engineering/security#sirt---security-incident-response-team) team is on-call 24/7/365 to assist with any security incidents. Information about SecOps on-call responsibilities and incident ownership is available in the [On-Call Guide](../secops-oncall.html).

## <i class="fas fa-receipt" id="biz-tech-icons"></i> Incident Management and Review

As part of the incident management and review process the SIRT maintains a recurring meeting that takes place on Monday of each week. During this meeting all of the previous weeks incidents, and any incidents that are currently open are reviewed. The review process covers the incident's scope, impact, the work performed to mitigate and remediate the incident, next steps, blockers, and current status. These meetings are also an opportunity to discuss mishandled incidents and process improvements.

### Incident Identification

Security incident investigations are initiated when a security event has been detected on [GitLab.com](https://www.gitlab.com) or as part of the GitLab company. These investigations are handled with the same level of urgency and priority regardless of whether it's a single user or multiple projects. 

Incident indicators can be reported to SIRT either internally, by a GitLab team member, or [externally](/handbook/engineering/security/#external-contact-information). It is the Security team's responsibility to determine when to investigate dependent on the identification and verification of a security incident.

The GitLab Security team identifies security incidents as any violation, or threat of violation, of GitLab security, acceptable use or other relevant policies.

### Incident Response Process - this guide covers the following activities for all identified security incidents:

1. Detection 
- The SIRT, other internal, or external entity identifies a Security or Privacy 
- Event that may be the result of a potential exploitation of a Security Vulnerability or Weakness, or that may the result of an innocent error. 
- One of our Security detection controls identifies event outside of the established security baseline 
2. Analysis
- SIRT determines whether the reported security or privacy event is in actuality security or a privacy event 
- SIRT determines the incident severity based on the following [risk matrix](/handbook/engineering/security/security-assurance/security-risk/storm-program/index.html#security-operational-risk-management-program)
3. Containment
- Mitigates the root cause of the incident to prevent further damage or exposure 
- SIRT may implement additional controls to minimize the damage as a  result of the incident
- Determine if it is safe to continue operations with the affected system 
- Permit or deny the operations of the affected system 
4. Eradication 
- Components that have caused the security incident are eliminated 
- Removal of the attackers’ access to the environment or the targeted system
- Strengthen the the controls surrounding the affected system 
5. Recovery
- Represents the effort to restore the affected system’s operations after the problem that gave rise to the incident has been corrected 
- Implementation of additional monitoring controls
- Update the incident record with any relevant details 
6. Post-Incident analysis and activities 
- Post Mortem and lessons learned activity 
 

### Confidentiality

Security incidents may (and usually do) involve sensitive information related to GitLab, GitLab's customers or employees, or users who (in one way or another) have engaged with GitLab. GitLab, while codifying the [Transparency](/handbook/values/#transparency) value, also strongly believes in and strives to maintain the privacy and confidentiality of the data its employees, customers, and users have entrusted us with.

A **confidential** issue means any data within the issue and any discussions about the issue or investigation are to be kept to **GitLab employees only** unless permission is explicitly granted by GitLab Legal, a GitLab Security Director, the VP of Security, or the GitLab Executive Team.

### Incident Tracking

Security incident investigations must begin by opening a [tracking issue](https://gitlab.com/gitlab-com/gl-security/security-operations/sirt/operations/-/issues/new?issuable_template=Incident_Response) in the [SIRT](https://gitlab.com/gitlab-com/gl-security/security-operations/sirt/operations/-/issues) project and using the Incident Response template. This tracking issue will be the primary location where all work and resulting data collection will reside throughout the investigation.

All artifacts from an investigation must be handled per the [Artifact Handling and Sharing](https://gitlab.com/gitlab-com/gl-security/runbooks/-/blob/master/sirt/external_requests/handling_and_sharing_artifacts.md) internal only runbook.

**NOTE:** The tracking issue, any collected data, and all other engagements involved in a Security Incident must be kept **strictly confidential**.

### Incident Severity

Assigning severity to an incident isn't an exact science and it takes some rational concepts mixed with past experiences and gut feelings to decide how bad a situation may be. When considering severity, look at:

- The type of data involved and how it's classified using the [Data Classification Policy](/handbook/engineering/security/data-classification-standard.html)
    - Was this data leaked or disclosed to parties who should not have visibility to it?
    - Was the data been modified in our records? (either confirmed or believed to be)
- Was a user or service account taken over?
    - What level of access did this account have and to what services or hosts?
    - What actions were taken by the compromised account?
- If a vulnerability is present on a host or service, consider the impact it might have on GitLab and the likelihood of it being exploited by using the [Risk Factors and Risk Scoring](/handbook/engineering/security/security-assurance/security-risk/storm-program/index.html#security-operational-risk-management-program) documentation.
    - Was the vulnerability exploited? If so, how was it used and how frequently?
- What is the scope of the incident?
    - How many GitLab.com users were/may have been impacted?
    - How many hosts or services?
- Has this incident resulted in any hosts or services being unavailable?

To help place the correct severity rating on the incident you are about to submit, please refer to the following examples:

| Severity  | Description  | Examples  |Resolution   |   
|---|---|---|---|
| High | A Critical Incident with a High Impact | 1. Gitlab.com is down for all customers <br><br></n>2. Confidentiality or Privacy is breached <br><br> 3. Customer Data is lost <br><br></n>4. Exposed key |  Activate Pager Duty Immediately |  
| Low | A minor incident with a very low impact  | Suspicious activity on team-member laptop <br><br>  Third party vendor vulnerability <br><br> [Example_1](https://gitlab.com/gitlab-com/gl-security/security-operations/sirt/operations/-/issues/1414)<br><br>[Example_2](https://gitlab.com/gitlab-com/gl-security/security-operations/sirt/operations/-/issues/1469) <br><br> [Example_3](https://gitlab.com/gitlab-com/gl-security/security-operations/sirt/operations/-/issues/1485) | Resolution will be provided during business hours for Engineer on call |


### Internal Engagement & Escalation

Coordinate with internal teams and prepare for the incident investigation:

- Invite all available parties to the SIRT incident response Zoom conference bridge for easier discussion (see topic in the SIRT slack channel, or zoom link in incident issue). The Security Engineer On-Call will begin recording the Zoom conference bridge **to their computer** then upload it to the team drive after concluding the incident bridge.
- Open an incident-focused Slack channel to centralize non-verbal discussion, particularly if the incident is of a sensitive nature. This should follow the naming convention `#sirt_####` where #### is the GitLab issue number in the SIRT project.
- If the incident was created by the security pager, a Google Drive Folder and Shared Google doc should have been created automatically and linked to the issue. If the incident was created manually:
    - Set up a [shared Google Drive folder or gcs bucket](https://gitlab.com/gitlab-com/gl-security/runbooks/-/blob/master/sirt/external_requests/handling_and_sharing_artifacts.md#storing-and-sharing-files-using-google-cloud-storage) for centralized storage of evidence, data dumps, or other pieces of critical information for the incident.
    - Create a shared Google Doc from the [Security Incident Response Template](https://docs.google.com/document/d/1mYQxuLXGaBr6xwHV7YgRbDt_k597tk_Dr2NFX4qb79s/template/preview) and move it into the shared Google Drive folder to act as a centralized record of events in real-time. Try to capture significant thoughts, actions, and events as they're unfolding. This will simplify potential hand-off's and eventual Incident Review of the incident.

In the event that an incident needs to be escalated within GitLab, the Security Engineer On Call (SEOC) will page the Security Incident Manager On Call (SIMOC). It is the responsibility of the SIMOC to direct response activities, gather technical resources from required teams, coordinate communication efforts with the Communications Manager On Call, and further escalate the incident as necessary.

Characteristics of an incident requiring escalation include but are not limited to the following:

- Incidents involving or likely to involve data with an Orange or Red classification
- Incidents that are likely to impact, or are actively impacting the availability or functionality of essential services
- Incidents affecting legal or financial resources
- Incidents that are likely to require a breach notification or public notification
- Incidents involving criminal activity or that may require the involvement of law enforcement
- Incidents involving key personnel such as executive leadership

If applicable, coordinate the incident response with [business contingency activities](/handbook/business-ops/gitlab-business-continuity-plan/).

### Containment

Once an incident has been identified and the severity has been set, the incident responder must attempt to limit the damage that has already occurred and prevent any further damage from occurring. When an incident issue is opened, it will automatically contain the `~Incident::Phase::Identification` label. At the start of the containment phase this label will be updated to `~Incident::Phase::Containment`.

The first step in this process is to identify impacted resources and determine a course of action to contain the incident while potentially also preserving evidence. Containment strategies will vary based on the type of incident but can be as simple as marking an issue confidential to prevent information disclosure or to block access to a network segment.

It's important to remember the containment phase is typically a stop-gap measure to limit damage and not to produce a long term fix for the underlying problem. Additionally the impact of the mitigation on the service must be weighed against the severity of the incident.

When triaging priority::1/severity::1 incidents there may be times that SIRT or Infrastructure are unable to mitigate an issue, or identify the full impact of a potential mitigation. In these cases the [Development Escalation Process](/handbook/engineering/development/processes/Infra-Dev-Escalation/process.html) can be used to engage with the development team on-call. It is important that this process is followed [as documented](/handbook/engineering/development/processes/Infra-Dev-Escalation/process.html#process-outline) and only for priority::1/severity::1 issues.

### Remediation and Recovery

During the remediation and recovery phase the incident responder will work to ensure impacted resources are secured and prepared to return the service to the production environment. This process may involve removing malicious or illicit content, updating access controls, deploying patches and hardening systems, redeploying systems completely, or a variety of other tasks depending on the type of incident. When transitioning from the containment phase into the remediation phase the SEOC will update the phase lable to `~Incident::Phase::Eradication` and when the remediation is complete the label will be updated to `~Incident::Phase::Recovery`.

A Incident Review will be completed for all `severity::1` incidents to guide the remediation and recovery process. Careful planning is required to ensure successful recovery and prevention of repeat incidents. The incident responder coordinates impacted teams to test and validate all remediations prior to deployment.

This phase should prioritize short term changes that improve the overall security of impacted systems while the full recovery process may take several months as longer term improvements are developed. During the post remediation Incident Review process the incident phase label will be updated to `~Incident::Phase::Incident Review`.

### Resolution

Upon completing the containment, remediation, communication and verification of impacted services, the incident will be considered resolved and the incident issues may be closed and the incident phase label will be changed to `Incident::Phase::Closed`.

The incident response process will move on to a post-mortem and lessons learned phase through which the process improvements and overall security of the organization can be analyzed and strengthened.

### Internal & External Communication

Our [security incident communication plan](/handbook/engineering/security/security-operations/sirt/security-incident-communication-plan.html) defines the who, what, when, and how of GitLab in notifying internal stakeholders and external customers of security incidents.

### Engaging Law Enforcement

If during the course of investigating a security event the incident itself, materials involved in the incident (stored data, traffic/connections, etc), or actions surrounding the incident are deemed illegal in the United States, it may be necessary (and advisable) to engage U.S. law enforcement.

1. The Security Engineer On-Call will immediately escalate to the Director of Security Operations to raise awareness of the legal concern.
1. Following review, the Engineer and Director will engage the VP of Security and VP of Legal for validation of next steps.
1. The Director of Security Operations will then contact the appropriate law enforcement agency.

### When You Join an Incident Channel or Call

In the event of a perceived major security incident (which may prove to not be one at a later point), adhoc communication is sometimes required for coordination. This is outlined in the sections above. If you are identified as someone who could assist during the perceived security incident with either the identification, confirmation, or mitigation of the incident, you will be added to a dedicated Zoom call or Slack channel. Upon joining that call/channel, please take note of the following:

- This is crisis management communication channel, that means that it's **private by default**. It may contain business critical or PII information that cannot be shared with the larger company at this time, or ever. Should GitLab Security determine that the content of this particular communication channel can be made internally available or public at a later point, the required changes will be made.
- **Read the channel history before asking questions**. Get some context, read through past conversations and related documents. The relevant person will reach out to you with specific asks at the right time.
- **Do your best to stay professional**. However, be aware that security incidents are often stressful and we're all humans. People may raise their voice, or use wording that seems unnecessary, harsh or inappropriate. It's important to cut people some slack (no pun intended) during this stressful time, and raise/address any potentially erratic behavior with the relevant manager once the incident is over.
- **Humor is your ally**. No, it really is.


#### Incident tracking

Incidents are tracked in the [Operations tracker](https://gitlab.com/gitlab-com/gl-security/security-operations/sirt/operations/) through the use of the [incident template](https://gitlab.com/gitlab-com/gl-security/security-operations/sirt/operations/-/blob/master/.gitlab/issue_templates/incident_response.md).

The correct use of dedicated **scoped incident labels** is critical to the sanity of the data in the incident tracker and the subsequent metrics gathering from it.

#### Incident labels description

* **Incident delineator** `incident` - denotes that an issue should be considered an incident and tracked as such.
* `Incident::Phase` - what stage is the incident at?
    * `Incident::Phase::Identification` - Incident is currently being triaged (log dives, analysis, and verification)
    * `Incident::Phase::Containment` - Limiting the damage (mitigations being put in place)
    * `Incident::Phase::Eradication` - Cleaning, restoring, removing affected systems, or otherwise remediating findings
    * `Incident::Phase::Recovery` - Testing fixes, restoring services, transitioning back to normal operations
    * `Incident::Phase::IncidentReview` - The incident review process has begun (required for all S1/P1 incidents)
    * `Incident::Phase::Closed` - Incident is completely resolved
* `Incident::Category` - what is the nature of the incident?
    * `Incident::Category::Abuse` - Abusive activity impacted GitLab.com
    * `Incident::Category::CustomerRequest`
    * `Incident::Category::DataLoss` - Loss of data
    * `Incident::Category::InformationDisclosure` - Confidential information might have been disclosed to untrusted parties
    * `Incident::Category::LostStolenDevice` - Laptop or mobile device was lost or stolen
    * `Incident::Category::Malware` - Malware
    * `Incident::Category::Misconfiguration` - A service misconfiguration
    * `Incident::Category::NetworkAttack` - Incident due to malicious network activity - DDoS, cred stuffing
    * `Incident::Category::NotApplicable` - Used to denote a false positive incident (such as an accidental page)
    * `Incident::Category::Phishing` - Phishing
    * `Incident::Category::UnauthorizedAccess` - Data or systems were accessed without authorization
    * `Incident::Category::Vulnerability` - A vulnerability in GitLab and/or a service used by the organization has lead to a security incident
* `Incident::Organization` - what is impacted?
    * `Incident::Organization::AWS` - One of GitLab's AWS environments
    * `Incident::Organization::Azure` - GitLab's Azure environment
    * `Incident::Organization::GCP` - GitLab's GCP environment
    * `Incident::Organization::GCPEnclave` - GitLab Security's GCP environment
    * `Incident::Organization::GSuite` - Google Workspaces (GSuite, GDrive)
    * `Incident::Organization::DO` - Digital Ocean environment
    * `Incident::Organization::GitLab` - GitLab the organization and GitLab the product
    * `Incident::Organization::SaaS` - Incident in vendor-operated SaaS platform
    * `Incident::Organization::GitLabPages` - GitLab.com Pages
    * `Incident::Organization::EnterpriseApps` - Other enterprise apps not defined here (Zoom, Slack, etc)
* `Incident::Source` - how did SIRT learn of the incident?
* `Incident::Origin` - how did GitLab learn of the incident?
* `Incident::Classification` - how accurate was the finding?
    * `Incident::Classification::TruePositive`
    * `Incident::Classification::FalsePositive`
    * `Incident::Classification::TrueNegative`
    * `Incident::Classification::FalseNegative`

