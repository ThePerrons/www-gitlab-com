---
layout: markdown_page
title: "Channel Operations"
description: "This page serves as the Channel Operations team page and includes standard channel reporting links, and details for managing Channel opportunities"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
 {:toc .hidden-md .hidden-lg}
 
 
<link rel="stylesheet" type="text/css" href="/stylesheets/biztech.css" />
{::options parse_block_html="true" /}



# Welcome to the Channel Operations page

## _Pinned Announcement_
New partner program changes are effective on August 15, 2021. Please visit the [Channel Ops Document Library](https://docs.google.com/document/d/1K12I8-UOuC6f92duKB24iypOo_igox1bQMMbUooqbx0/edit) for updates and collateral. For information about the program (not operational), visit the [Channel Partner Handbook](https://about.gitlab.com/handbook/resellers/).
 
## Meet the Team
### Who We Are
- Colleen Farris - Director, Channel Operations
- Emily Murphy - Manager, Alliance Operations
- Kim Stagg - Manager, Channel Operations
- Niles Jamshaid - Manager, Channel Operations
- Dennis Zeissner - Associate Partner Operations Analyst
- Pablo Vargas - Channel Operations Analyst. 

### How to Contact Us
The **#channel-programs-ops** Slack channel can be leveraged for inquiries. Both the Channel Operations Team and the Channel Programs Team monitor this slack channel.
If you are reporting a problem or have suggestions, changes, or similar, please open an issue on the [Channel Operations Issue Board](https://gitlab.com/groups/gitlab-com/-/boards/2552402?&label_name[]=Channel%20Ops) for operational issues, or the [Channel Team’s Issue Board](https://gitlab.com/groups/gitlab-com/-/boards/1508300?label_name[]=Channel) for program issues.
 
### The Channel Operations Issue Board
On the [Channel Operations Issue Board](https://gitlab.com/groups/gitlab-com/-/boards/2552402?&label_name[]=Channel%20Ops), each column represents a type of request (feature request, alliances, data & reporting, etc.). When you submit a request to the Channel Operations board, the team will assign the issue and add the corresponding tags. The Channel Operations Board also uses progress tags on issues to show the status of open issues. Each issue is updated regularly with notes and progress tags and should be checked before reaching out to the team for status updates. 
 
To open an issue with Channel Operations and select [New Issue](https://gitlab.com/gitlab-com/sales-team/field-operations/channel-operations/-/issues).
 
## Standard Channel Reporting Links
 
<details>
<summary markdown='span'>
Channel & Alliance SFDC Dashboards
</summary>
- [Global Channel Dashboard](https://gitlab.my.salesforce.com/01Z4M000000oYpV): Dashboard that contains standard metrics for measuring overall Channel performance
- [Channel Forecast Dashboard](https://gitlab.my.salesforce.com/01Z4M000000oYLJ): Dashboard that contains comparison between `Partner Sourced and SQS = Channel` pipe and bookings
- [Channel Commercial Dashboard](https://gitlab.my.salesforce.com/01Z4M000000oYe4): Dashboard that is focused specifically on our Commercial segment
- [Alliances Dashboard](https://gitlab.my.salesforce.com/01Z4M000000oYAp): Dashboard that contains standard metrics for measuring Alliance Partner performance
</details>
<details>
<summary markdown='span'>
Google Sheet Reporting
</summary>
- [Channel & Alliances Weekly Forecast Call Analysis](https://docs.google.com/spreadsheets/d/1t_9VbB_5vjMOAL_Rs7syPCJ4D8Argl6f6Tv39yuNDN0/edit#gid=1119532428): Standard metrics for the weekly Sales Forecast Call, updated weekly
- [GitLab Certification Tracking Dashboard](https://docs.google.com/spreadsheets/d/147DUeV4k2ybqftcnJcBR6SOoZWxGpjoCfNyn1hNVfAg/edit#gid=569795884): Dashboard showing Partner certification data, updated weekly
</details>
<details>
<summary markdown='span'>
Sisense Reporting
</summary>
- [Channel Analysis Dashboard](https://app.periscopedata.com/app/gitlab/820617/WIP---Channel-Analysis-Dashboard): Overall Global Channel Performance specifically focused on Partner Sourced opportunities
</details>
### Reports by Territory
<details>
<summary markdown='span'>
AMER
</summary>
- [Channel Book & Pipe - Current Fiscal Quarter](https://gitlab.my.salesforce.com/00O4M000004aqxE)
- [Channel Book & Pipe by Partner - Current Fiscal Quarter](https://gitlab.my.salesforce.com/00O4M000004aqxJ)
- [Deal Path Pipe & Book - Current Fiscal Quarter](https://gitlab.my.salesforce.com/00O4M000004aqxO)
- [Open Partner Sourced Pipeline by Partner - Current Fiscal Quarter](https://gitlab.my.salesforce.com/00O4M000004aqxT)
- [Partner Sourced Bookings by Partner - Current Fiscal Quarter](https://gitlab.my.salesforce.com/00O4M000004aqxY)
- [Partner Sourced Creation by Partner - Current Fiscal Quarter](https://gitlab.my.salesforce.com/00O4M000004aqxd)
- [Partner Sourced New Logos - Current Fiscal Quarter](https://gitlab.my.salesforce.com/00O4M000004aqxs)
- [Channel Renewals - Current & Next Fiscal Quarter](https://gitlab.my.salesforce.com/00O4M000004aqy2)
</details>
<details>
<summary markdown='span'>
APAC
</summary>
- [Channel Book & Pipe - Current Fiscal Quarter](https://gitlab.my.salesforce.com/00O4M000004al8o)
- [Channel Book & Pipe by Partner - Current Fiscal Quarter](https://gitlab.my.salesforce.com/00O4M000004al93)
- [Deal Path Pipe & Book - Current Fiscal Quarter](https://gitlab.my.salesforce.com/00O4M000004al9I)
- [Open Partner Sourced Pipeline by Partner - Current Fiscal Quarter](https://gitlab.my.salesforce.com/00O4M000004al9X)
- [Partner Sourced Bookings by Partner - Current Fiscal Quarter](https://gitlab.my.salesforce.com/00O4M000004al9r)
- [Partner Sourced Creation by Partner - Current Fiscal Quarter](https://gitlab.my.salesforce.com/00O4M000004alAk)
- [Partner Sourced New Logos - Current Fiscal Quarter](https://gitlab.my.salesforce.com/00O4M000004alB4)
- [Channel Renewals - Current & Next Fiscal Quarter](https://gitlab.my.salesforce.com/00O4M000004alBT)
</details>
<details>
<summary markdown='span'>
EMEA
</summary>
- [Channel Book & Pipe - Current Fiscal Quarter](https://gitlab.my.salesforce.com/00O4M000004al8t)
- [Channel Book & Pipe by Partner - Current Fiscal Quarter](https://gitlab.my.salesforce.com/00O4M000004al98)
- [Deal Path Pipe & Book - Current Fiscal Quarter](https://gitlab.my.salesforce.com/00O4M000004al9N)
- [Open Partner Sourced Pipeline by Partner - Current Fiscal Quarter](https://gitlab.my.salesforce.com/00O4M000004al9c)
- [Partner Sourced Bookings by Partner - Current Fiscal Quarter](https://gitlab.my.salesforce.com/00O4M000004al9w)
- [Partner Sourced Creation by Partner - Current Fiscal Quarter](https://gitlab.my.salesforce.com/00O4M000004alAp)
- [Partner Sourced New Logos - Current Fiscal Quarter](https://gitlab.my.salesforce.com/00O4M000004alB9)
- [Channel Renewals - Current & Next Fiscal Quarter](https://gitlab.my.salesforce.com/00O4M000004alBY)
</details>
<details>
<summary markdown='span'>
PUB-SEC
</summary>
- [Channel Book & Pipe - Current Fiscal Quarter](https://gitlab.my.salesforce.com/00O4M000004al8y)
- [Channel Book & Pipe by Partner - Current Fiscal Quarter](https://gitlab.my.salesforce.com/00O4M000004al9D)
- [Deal Path Pipe & Book - Current Fiscal Quarter](https://gitlab.my.salesforce.com/00O4M000004al9S)
- [Open Partner Sourced Pipeline by Partner - Current Fiscal Quarter](https://gitlab.my.salesforce.com/00O4M000004al9h)
- [Partner Sourced Bookings by Partner - Current Fiscal Quarter](https://gitlab.my.salesforce.com/00O4M000004alA1)
- [Partner Sourced Creation by Partner - Current Fiscal Quarter](https://gitlab.my.salesforce.com/00O4M000004alAu)
- [Partner Sourced New Logos - Current Fiscal Quarter](https://gitlab.my.salesforce.com/00O4M000004alBJ)
- [Channel Renewals - Current & Next Fiscal Quarter](https://gitlab.my.salesforce.com/00O4M000004alBd)
</details>
 
For a global view of current and next fiscal quarter channel renewals, [click here](https://gitlab.my.salesforce.com/00O4M000004aWY3).
_All required team reporting is included above. In the event you need a special report, please [open an issue](https://gitlab.com/gitlab-com/sales-team/field-operations/channel-operations/-/issues) on the Channel Operations Board._
_For the reporting training video hosted by the Channel Operations team, as well as quick reference “cheats” to help with your Salesforce reporting, [click here](https://docs.google.com/document/d/1F1O3BUX80SJIqaFD9TF2RuyZ7DXB865AuiwaGctHnGo/edit?usp=sharing)._
 
## Standard Channel Practices
For detailed information on GitLab’s Channel Partner Program, visit the [Channel Partner Handbook](https://about.gitlab.com/handbook/resellers/). Partners must be an Authorized GitLab Partner _and have completed one sales certification_ to transact any GitLab products or services. To achieve authorization, partners must have an executed agreement and meet the requirements of the GitLab Partner Program. Only GitLab partners in good standing may sell GitLab products and services unless specifically approved on a case-by-case basis by the GitLab partner program team. Partners must [sign up](https://partners.gitlab.com/) to be authorized. 
 
### Policy and Process
All [**Partner Co-Sell**](https://about.gitlab.com/handbook/sales/field-operations/channel-operations/#partner-co-sell) opportunities require an authorized GitLab Partner(s) to transact the deal. Deal Registration is not applicable for Partner Co-Sell opportunities. 
[**Partner Sourced Deal Registration**](https://about.gitlab.com/handbook/sales/field-operations/channel-operations/#partner-sourced-deal-registration) opportunities also require an authorized GitLab Partner to transact, and must also be found/created and registered in the [GitLab Partner Portal](https://partners.gitlab.com/) (excludes Alliances and GSIs). The applicable GitLab Channel Managers must approve the registered opportunity to qualify for partner program discounts and incentives. Partner Sourced Deal Registration opportunities are either:
1. Net-new to GitLab
2. An upsell/add-on for an existing GitLab customer
Partner Sourced Deal Registration logic needs to match `Sales Qualified Source = Channel Generated` on the opportunity.  

For more information: [Deal Registration Program Overview](https://about.gitlab.com/handbook/resellers/#the-deal-registration-program-overview).
 
 
### Transacting Through Distribution
As of May 3, 2021, all partners in India must transact through our distribution partner, Redington. 
For the US and EMEA, we encourage Open Partners to purchase through Distribution, but it is not a requirement.  

All US PubSec deals are transacted through Carahsoft or Immix (GSA only). 
 
 
### Quoting Requirements for Channel Deals
When a deal is being transacted via the Channel, and a discount other than the defined Program discounts are being offered, a GitLab quote is ***required***. At a minimum, a [Draft Proposal](https://about.gitlab.com/handbook/sales/field-operations/sales-operations/deal-desk/#how-to-create-a-draft-proposal) needs to be created and provided to the Partner.
Discounted quotes not in SFDC and sent to a Partner are not permitted within any GEO or Sales Segment. This includes providing product and discounted pricing quote details in an email.
 
### Opportunity Requirements to Request a Quote
Channel Managers or Sales Reps are responsible for quoting all GitLab direct and reseller-direct quotes. For quotes going through distribution, chatter @Partner Help Desk on the opportunity with the following information:
- For opportunities that include a Partner Sourced Deal Registration, this registration [must be linked to the opportunity](https://about.gitlab.com/handbook/sales/field-operations/channel-operations/#partner-sourced-deal-registration-resale-opportunities) before requesting a quote
- Sold-To Contact: the email address the license will be sent to at the customer account
- Invoice Owner: The distributor processing the deal. If there’s no distributor on the deal, Channel Managers or Sales Representatives should be processing the quote
- Special Terms or Notes (if applicable)
- Date the licenses should start
- Product information: Premium or Ultimate, Self-Managed or SaaS, Number of seats, number of years
- Verified Discounts, especially if there are additional sales discounts outside of programmatic discounts
- For Renewals: Screenshot or other verification for true-ups
 
## SFDC Field Definitions
### Section I: Partner Sourced Deal Registration
- **DR - Partner**: The reseller partner that ‘submitted’ the Partner Sourced Deal Registration that the GitLab Sales Team subsequently ‘approved’ in the system.
- **DR - Distributor**: The GitLab-authorized distributor from which the DR - Partner is buying, when applicable.
- **DR - Partner Deal Type**:
  - _MSP_: The partner purchases _and owns_ the license on behalf of the customer.
  - _Resale_: The partner is actually transacting the deal on their paper.
  - _Referral_: The partner is bringing us the lead/opportunity but will either transact directly with GitLab or through another partner.
  - _DR - Status_: Dictates whether the Partner Sourced Deal Registration is approved or denied.
 
![1-PSDR_Definitions](/handbook/sales/field-operations/channel-operations/images/1-PSDR_Definitions.png)

 
### Section II: Primary Partner Quote Data (Closed-Won Order Data)*
- **Partner**: The resale partner on the primary quote; the transacting partner
- **Partner Track**: Either Select or Open based off of the resale partner
- **Distributor**: The GitLab-authorized distributor that transacts according to the primary quote:
- **Deal Type**:
  - _MSP_: The partner purchases _and owns_ the license on behalf of the customer.
  - _Resale_: The partner is actually transacting the deal on their paper.
  - _Referral_: The partner is bringing us the lead/opportunity but will either transact directly with GitLab or through another partner.
***Stamped from primary quote when approved. Fields are locked.***
 
![2-Primary_Quote_Definitions](/handbook/sales/field-operations/channel-operations/images/2-Primary_Quote_Definitions.png)
 
### Section III: Partner Contribution Data
- **Influence Partner**: Other partners, generally SI/GSIs or alliance partners, that have helped influence a deal but are not part of the actual transaction
- **Influence Partner**: Other partners, generally SI/GSIs or alliance partners, that have
- **Platform Partner**:  Customer's platform that GitLab is being deployed.
- **GCP Requests**
 
![3-Partner_Contribution_Definitions](/handbook/sales/field-operations/channel-operations/images/3-Partner_Contribution_Definitions.png)
 
### Services Resale
- Partners can earn a program discount for reselling GitLab Professional Services
 
### Incumbency Renewals
- Incumbent partners qualify for program renewal discounts. The partners that transact the most recent sale are considered the incumbent.
- A different partner can earn an incumbency discount only through formal written communications from the customer. This can be provided via email from an authorized representative from the customer.
- To earn partner discounts, partners will be required to meet compliance requirements (i.e. in good credit standing, have provided quarterly updates on the customer, review within 30 days of renewal, etc).
 
### Tender Offers
- Tender offers are ones where the customers are requesting multiple bids for a project.
- Since all partners would be engaged in the sales process and would be creating a bid, all partners qualify for the Partner Co-Sell discount. However, if a partner was early in with the customer (pre-tender) and sourced the deal, then they could receive the higher discount with an approved Partner Sourced Deal Registration. If the partner earning the Partner Sourced discount is not awarded the deal, they would not receive additional referral compensation. Any partner offering their own services would qualify for Service Attach incentives in addition to any sales discounts for which they would qualify.
  - The exception to the above would be for US PubSec business. In the event there is an approved Partner Sourced Deal Registration on a tender offer, the rest of the partners bidding would receive MSRP (List Price).
 
### Program Compliance
- For partners to transact according to program discounts, they must agree to the GitLab Partner Agreement.
- To earn partner discounts, partners are required to be program compliant.
- Non-contracted partners may transact on a one-off basis, only with explicit approval of channel leadership.
 
## Unauthorized Partners
- Unauthorized partners have not signed a GitLab Partner Agreement.
- If an unauthorized partner would like to transact GitLab products or services, please have them visit the [Partner Portal](https://partners.gitlab.com/) to sign up. Someone who has authority to accept the Agreement is required.
- Unauthorized partners cannot transact GitLab products and/or services, unless rare but explicit approval is granted. Please reach out to the #channel-programs-ops Slack channel.
- A key goal of the GitLab Channel Program is the success of our authorized partners. We are developing our channel to provide coverage across geos, customer segments and vertical markets.
 
 
## Channel Reporting and Tagging
![4-Ch_Reporting_Tagging](/handbook/sales/field-operations/channel-operations/images/4-Ch_Reporting_Tagging.png)
 
### Definitions
1. **Deal Path**: How the deal is transacted. Values can be Channel, Direct, or Web. Includes Referral Ops for Channel.
2. **Partner Sourced Deal Reg**: Partner submits a Registration for their sourced opportunity via the Partner Portal. For the purposes of this matrix the assumption is the Deal Reg is approved. If the deal is not Partner Sourced then Deal Reg does not apply.
3. **Initial Source**: SFDC Lead value that is populated based on lead source. Will default to CQL (Channel Qualified Lead) when a Partner submits a Partner Sourced Deal Reg and an Opportunity does not already exist in the system.
4. **SQS (Sales Qualified Sourced)**: Who converts/creates the Opportunity in SFDC. Can only be 1 value
5. **Deal Type**: Whether or not the partner is taking paper or not
6. **Order Type**: Customer order designation in SFDC. New First Order or Growth
 
 
### Use Cases
- **Numbers 1 & 4**
  - Channel Partner submits Partner Sourced Deal Reg and no Opportunity exists in the system. Therefore, the Initial source is CQL and SQS defaults to Channel Generated.
  - This applies to both New and Growth orders
- **Number 2**
  - Channel submits an order for a deal that we did not have an opportunity in the system for but did not submit a Partner Sourced Deal Reg
- **Number 3**
  - Deal is transacting thru the channel but it was sourced by either a GitLab AE or SDR
- ***Exception***
  - AE or SDR creates an opportunity prior to a valid Partner Sourced Deal Reg being submitted and approved. *The Channel Manager needs to escalate for an exception by adding a chatter message on the applicable opportunity and tagging Colleen Farris. 
 
### Default Logic
1. If `Partner Sourced Deal Reg = True` and no opportunity exists, then `Initial Source = CQL` > `SQS = Channel Generated`
2. Alliances and OEM Logic: No Deal Registration applied but if `Initial Source = CQL` then
`SQS = Channel Generated`
 
### SFDC Opportunity Source Field Values for Channel
- **Initial Source**:
  - _Channel Qualified Lead (CQL)_: GitLab Channel Partner created and/or qualified the Lead whether they sourced it themselves or GitLab provided the inquiry to them to work.
- **Sales Qualified Source**:
  - _Channel_: The Channel Partner has converted the Lead/CQL to a qualified opportunity, or has created a brand new opportunity without a prior lead being in the system. This field defaults to Channel when `Initial Source = CQL`.
- **DR - Deal Engagement**:
  - _Partner Sourced_: Partner has either found the original opportunity or an upsell to a current customer. If the `Initial Source  = Channel Qualified Lead` or `Sales Qualified Source = Channel Generated`, then the deal is Partner Sourced.
*Please visit the [Marketing Handbook Page](https://about.gitlab.com/handbook/marketing/marketing-operations/#initial-source) for Intial Source definition and context.
 
 
## Managing Partner Opportunities
[Partner Co-Sell (Resale)](https://about.gitlab.com/handbook/sales/field-operations/channel-operations/#partner-co-sell) | Partner Sourced Deal Registration: [Resale](https://about.gitlab.com/handbook/sales/field-operations/channel-operations/#partner-sourced-deal-registration-resale-opportunities) | [MSP](https://about.gitlab.com/handbook/sales/field-operations/channel-operations/#partner-sourced-deal-registration-msp-opportunities) | [Referral](https://about.gitlab.com/handbook/sales/field-operations/channel-operations/#partner-sourced-deal-registration-referral-opportunities) 
 
In order to transact with GitLab, a partner must both be authorized by GitLab, and have completed at least one sales training.
 
### Partner Co-Sell
All Channel deals are considered Partner Co-Sell opportunities unless there is an approved Partner Sourced Deal Registration. These opportunities are not sourced solely by a partner, but highlight the relationship between GitLab and partners in the selling process. Partner Co-Sell deals do not require the partner to submit a deal registration, and should be processed according to standard Partner Co-Sell discounts as identified in the [GitLab Partner Program](https://handbook/resellers/).
 
### Partner Sourced Deal Registration
There are three types of Partner Sourced Deal Registrations within the GitLab Partner Program. For step-by-step instructions on how to process each of these opportunities, see the specific sections: [Resale](https://about.gitlab.com/handbook/sales/field-operations/channel-operations/#partner-sourced-deal-registration-resale-opportunities), [MSP](https://about.gitlab.com/handbook/sales/field-operations/channel-operations/#partner-sourced-deal-registration-msp-opportunities), and [Referral](https://about.gitlab.com/handbook/sales/field-operations/channel-operations/#partner-sourced-deal-registration-referral-opportunities) .
 
The Partner Sourced Deal Registration program not only rewards partners for bringing net-new business to GitLab, but also rewards partners for selling their own services into the customer account.
 
Channel Managers are responsible for reviewing Partner Sourced Deal Registrations and must either approve, deny, or return the registration for additional information. It is recommended that the Channel Manager communicate with the applicable GitLab Sales Team Member during this process.
 
**The SLA for GitLab to respond to partners on a Partner Sourced Deal Registration is 48 hours. There must be contact with the registering partner within 48 hours, whether it be approval, denial, or a request for more information.** 
 
While multiple partners can submit a deal registration for an opportunity, only one Partner Sourced Deal Registration can be approved for a specific opportunity. As a reminder, Partner Sourced Deal Registrations are opportunity-based and partners cannot register an account.
 
### Partner Sourced Deal Registration: What to Expect
To view individual processing steps for each type of Partner Sourced Deal Registration Opportunity, click the appropriate type of deal: Partner Sourced Deal Registrations for [Resale](https://about.gitlab.com/handbook/sales/field-operations/channel-operations/#partner-sourced-deal-registration-resale-opportunities) | [MSP](https://about.gitlab.com/handbook/sales/field-operations/channel-operations/#partner-sourced-deal-registration-msp-opportunities) | [Referral](https://about.gitlab.com/handbook/sales/field-operations/channel-operations/#partner-sourced-deal-registration-referral-opportunities).  

_Note: The Partner Portal is hosted by Impartner and has SSO enabled with Vartopia, the partner-facing Deal Registration system. GitLab uses a third-party managed services team to help manage administrative processes for Deal Registration._. 

GitLab has three Deal Registration programs: Partner Sourced Deal Registration for resale opportunities, MSP Deal Registration for partners providing their own managed services, and Referral Deal Registrations for partners who do not transact the business they bring to GitLab.
 
### Partner Sourced Deal Registration: How it Works
When a partner submits a Partner Sourced Deal Registration, the Managed Services team will handle the initial submission. Upon record creation, the following occurs:
1. An email is sent to the partner acknowledging that the deal registration has been successfully created. The registration is also viewable in the deal registration section of the partner’s portal account.
2. The system notifies the Managed Services team to review the registration.
3. The system creates a SFDC record on the registration object that includes all of the details of the registration.  

The Managed Services team will evaluate the registration, link it to the appropriate customer account and contact in SFDC, and then send an email to the applicable Channel Manager to approve/deny the registration.  

By default, the Channel Manager is assigned as the Account Owner in the Partner Account in SFDC. GitLab’s Managed Services team will manually reassign the registration to the correct owner in cases where there may be more than one owner due to segment or territory. There is a 4 hour SLA for the Managed Services team once the Deal Registration has been submitted by the Partner.  

Once a Channel Manager is assigned a Partner Sourced Deal Registration, they receive an email notification. Channel Managers can also view the registrations in their list view within the SFDC Registration tab. To view the details of the Registration they can either click on the link in the email or the specific record in the Registration view in SFDC.
 
### Rules of Engagement for Partner Sourced Deal Registration
- To receive a quote with a Partner Sourced discount, they must complete a deal registration for the opportunity and it must be approved by a GitLab Team Member.
- Only one partner can earn a Partner Sourced Deal Registration discount per opportunity. Partners, other than the partner granted the deal registration discount, will receive the Partner Co-Sell discount rate. Deal registration approval is based upon order of receipt of the registration, qualification of the opportunity, partner ability to deliver in-country/region support, and partner relationship with the customer. Final deal registration approval decision will be made by GitLab Channel after discussion with GitLab Sales.
- Approved deal registrations have a standard 90-day expiration from the date of original approval. (Deal Registration extensions beyond the initial 90 days approval are at the sole discretion of GitLab). To grant a standard 30-day extension, click the “Extend DR 30 Days” button. For non-standard extensions beyond 30 days, please slack #channel-programs-ops and include the link to the registration record in SFDC.
 
![5-Extend_DR](/handbook/sales/field-operations/channel-operations/images/5-Extend_DR.png)
 
- GitLab collaborates with partners holding the approved deal registration and is available to support partners throughout the entire sales process.
- In the event the engagement is dissolved, the GitLab Sales Rep will generally notify the partner by phone or email. GitLab will reconsider other qualified deal registrations submitted for this deal, in chronological order of submission. If there are no other registration requests submitted, the GitLab sales rep will typically initiate engagement with a reseller of the GitLab sales rep’s choosing. 
 
### Partner Sourced Deal Registration System Status and Information
- Deal Registration details will never override any information that the sales team forecasts on the Opportunity.
- There is a Deal Registration ID that can be used to track all Deal Registrations in the system.
- All Deal Registrations are individual objects in SFDC under Registrations. They do not come in as leads.
 
### Deal Registration Reporting & Tools
- [Deal Registration Report](https://gitlab.my.salesforce.com/00O4M000004aUal)
- [Deal Registration Object Fields & Definitions](https://drive.google.com/file/d/1pdPHZpR_0sOUJlat-USGvcXfFjumDwC9/view)
- [Adding the Registration Record to your Salesforce view](https://drive.google.com/file/d/1iUz42CfvYKPdw1quskc4NmraFVoZvFez/view)
- [Creating personal deal registration views](https://drive.google.com/file/d/1UtERcTNgNr9pTIf9OXDdpj3dNuhk9ISj/view)
- [Partner-facing Deal Registration instructions](https://about.gitlab.com/handbook/resellers/#deal-registration-instructions)
 
### Partner Sourced Deal Registration: Resale Opportunities
Partner Sourced Deal Registrations for resale opportunities reward partners for bringing net-new business to GitLab. With an approved Partner Sourced Deal Registration, a partner will receive a deeper discount than Partner Co-Sell opportunities, according to the [GitLab Partner Program(https://handbook/resellers/).
 
To process a Partner Sourced Deal Registration for a resale opportunity, follow the steps below:
 
1. Click the link in either the email or the [Deal Registration Report](https://gitlab.my.salesforce.com/00O4M000004aUal) to open the registration object in Salesforce.
2. Check to make sure that the Partner Sourced Deal Registration type says resale.
3. Click “Link/Create Opportunity.”
   ![9-Link_Create_Opp](/handbook/sales/field-operations/channel-operations/images/9-Link_Create_Opp.png)

4. On the Link/Create Opportunity page, first search for the opportunity in the provided list and/or by doing a “Global Search.”
  - **If the opportunity exists**, click “Select” next to the opportunity name. You will then be brought back to the deal registration record.
   ![6-Link_Opportunity_Select](/handbook/sales/field-operations/channel-operations/images/6-Link_Opportunity_Select.png)
  - **If there is no matching opportunity**, click “Create New,” and then choose “Standard” in the Opportunity Type. The system will then populate all the necessary fields, including the DR-mapped fields. Click “Save” and you will be brought back to the registration record.  
5. Click Approve/Deny/Return Registration.
   ![7-Approve_Deny_Return](/handbook/sales/field-operations/channel-operations/images/7-Approve_Deny_Return.png)
6. On the next page, choose approve, and if you have a message for the partner about the deal, you can add that into the “comments” section. This is not required, but anything included in this field will be sent back to the partner.
   ![8-Approve_Deny_Details.png](/handbook/sales/field-operations/channel-operations/images/8-Approve_Deny_Details.png)
   - If a distributor is involved, choose the distributor in the field that appears. If no distributor is involved, this field can be left blank.
   - Both the registration record and the opportunity record will be updated with the approval information.
7. If you have created a new opportunity during this process, chatter @sales-support and request that the opportunity owner be updated to the name of the sales team member who owns the customer account.
8. For quotes going through distribution, please chatter @Partner Help Desk with all the information for quote creation.
  - The deal registration form is not a quoting tool and will not have all the information needed to create the quote. You must get this information from the partner or elsewhere before requesting the quote.
 
### Partner Sourced Deal Registration: MSP Opportunities
Partner Sourced Deal Registrations for MSP opportunities reward partners for managing products and services for their customers accounts. With an approved Partner Sourced Deal Registration for MSP, a partner will receive a different discount than other resale opportunities, according to the [GitLab Partner Program](https://handbook/resellers/). 
 
A Managed Service Provider (MSP) purchases licenses on behalf of an end user. The MSP will be the owner and manager of the licenses but their customer, the end user, is the one using the licenses. This creates specific needs in GitLab Salesforce opportunities to ensure proper reporting and compensation. _Please note that the steps for an MSP opportunity differ from the steps for a resale opportunity._
 
To process a Partner Sourced Deal Registration for an MSP opportunity, follow the steps below:
 
1. Click the link in either the email or the [Deal Registration Report](https://gitlab.my.salesforce.com/00O4M000004aUal) to open the registration object in Salesforce.
2. Check to make sure that the Partner Sourced Deal Registration type says MSP.
3. Click “Link/Create Opportunity.”
   ![9-Link_Create_Opp](/handbook/sales/field-operations/channel-operations/images/9-Link_Create_Opp.png)
4. On the Link/Create Opportunity page, first search for the opportunity in the provided list and/or by doing a “Global Search.”
   - **If the opportunity exists**, click “Select” next to the opportunity name. You will then be brought back to the deal registration record.
   ![6-Link_Opportunity_Select](/handbook/sales/field-operations/channel-operations/images/6-Link_Opportunity_Select.png)
   - **If there is no matching opportunity**, click “Create New,” and then choose “Standard” in the Opportunity Type. The system will then populate all the necessary fields, including the DR-mapped fields. Click “Save” and you will be brought back to the registration record.
5. Click Approve/Deny/Return Registration.
   ![7-Approve_Deny_Return](/handbook/sales/field-operations/channel-operations/images/7-Approve_Deny_Return.png)
6. On the next page, choose approve, and if you have a message for the partner about the deal, you can add that into the “comments” section. This is not required, but anything included in this field will be sent back to the partner.
   ![8-Approve_Deny_Details.png](/handbook/sales/field-operations/channel-operations/images/8-Approve_Deny_Details.png)
   - If a distributor is involved, choose the distributor in the field that appears. If no distributor is involved, this field can be left blank.
   - Both the registration record and the opportunity record will be updated with the approval information.
6. Manually change the `Account Name`field to the partner name. This should **not** be the name of the customer.
7. If you have created a new opportunity during this process, chatter @sales-support and request that the opportunity owner be updated to the correct Sales Rep that owns the end-user account, even though the opportunity is created under the Partner MSP account.
8. **When creating a quote for an MSP opportunity, select the MSP quote template**
  - **Invoice owner**: The MSP Partner (same as DR-Partner referenced above).
  - **Special Terms and Notes**: Name of the End-user Customer (the MSP owns the licenses on behalf of).
9. For quotes going through distribution, please chatter @Partner Help Desk with all the information for quote creation.  

_The deal registration form is not a quoting tool and will not have all the information needed to create the quote. You must get this information from the partner or elsewhere before requesting the quote._
 
### Partner Sourced Deal Registration: Referral Opportunities
Partner Sourced Deal Registrations for referral opportunities reward partners for bringing net-new business to GitLab, even if that partner does not transact the deal. With an approved Partner Sourced Deal Registration for a referral, a partner will receive a **back-end rebate**, processed quarterly, according to the [GitLab Partner Program](https://handbook/resellers/).
 
To process a Partner Sourced Deal Registration for a referral opportunity, follow the steps:
 
1. Click the link in either the email or the [Deal Registration Report](https://gitlab.my.salesforce.com/00O4M000004aUal) to open the registration object in Salesforce.
2. Check to make sure that the Partner Sourced Deal Registration type says referral.
3. Click “Link/Create Opportunity.”
   ![9-Link_Create_Opp](/handbook/sales/field-operations/channel-operations/images/9-Link_Create_Opp.png)
4. On the Link/Create Opportunity page, first search for the opportunity in the provided list and/or by doing a “Global Search.”
   - **If the opportunity exists**, click “Select” next to the opportunity name. You will then be brought back to the deal registration record.
   ![6-Link_Opportunity_Select](/handbook/sales/field-operations/channel-operations/images/6-Link_Opportunity_Select.png)
   - **If there is no matching opportunity**, click “Create New,” and then choose “Standard” in the Opportunity Type. The system will then populate all the necessary fields, including the DR-mapped fields. Click “Save” and you will be brought back to the registration record.
5. Click Approve/Deny/Return Registration.
   ![7-Approve_Deny_Return](/handbook/sales/field-operations/channel-operations/images/7-Approve_Deny_Return.png)
6. On the next page, choose approve, and if you have a message for the partner about the deal, you can add that into the “comments” section. This is not required, but anything included in this field will be sent back to the partner.
   ![8-Approve_Deny_Details](/handbook/sales/field-operations/channel-operations/images/8-Approve_Deny_Details.png)
   - If a distributor is involved, choose the distributor in the field that appears. If no distributor is involved, this field can be left blank.
7. Both the registration record and the opportunity record will be updated with the approval information.
8. If you have created a new opportunity during this process, chatter @sales-support and request that the opportunity owner be updated to the name of the sales team member who owns the customer account.
9. For quotes going through distribution, please chatter @Partner Help Desk with all the information for quote creation.  

_The deal registration form is not a quoting tool and will not have all the information needed to create the quote. You must get this information from the partner or elsewhere before requesting the quote._
 
## Service Attach Opportunities
GitLab incentivizes partners that sell their own professional services into a customer environment at the time of a GitLab product sale. To be eligible for this back-end rebate, the partner needs to submit a Service Attached Deal Registration that is approved by a GitLab Channel Manager. This is separate from the Partner Sourced Deal Registration for the license sale.  

To track the Partner Services, the partner must register the deal on the [partner portal](https://about.gitlab.com/handbook/resellers/#gitlab-partner-portal).  

When a Channel Manager either receives an email alert, or sees a new registration in the [Deal Registration Report](https://gitlab.my.salesforce.com/00O4M000004aUal), the following steps should be taken:
 
1. Click the link in either the email or the [Deal Registration Report](https://gitlab.my.salesforce.com/00O4M000004aUal) to open the registration object in Salesforce.
2. Check to make sure that the Partner Sourced Deal Registration type says resale.
3. Click “Link/Create Opportunity.”
   ![9-Link_Create_Opp](/handbook/sales/field-operations/channel-operations/images/9-Link_Create_Opp.png)
4. On the Link/Create Opportunity page, first search for the opportunity in the provided list and/or by doing a “Global Search.”
   - **If the opportunity exists**, click “Select” next to the opportunity name. You will then be brought back to the deal registration record.
   ![6-Link_Opportunity_Select](/handbook/sales/field-operations/channel-operations/images/6-Link_Opportunity_Select.png)
   - **If there is no matching opportunity**, click “Create New,” and then choose “Standard” in the Opportunity Type. The system will then populate all the necessary fields, including the DR-mapped fields. Click “Save” and you will be brought back to the registration record.
   - **A Service Attach Deal Registration MUST attach to a license sale opportunity.** There will almost always be an opportunity in the system, and it is best practice for a Channel Manager to process the Partner Sourced Deal Registration first, if there is one, before processing the Service Attach Deal Registration.
   - **The opportunity must be either new or no older than 6 months old to qualify for the incentive.** If the appropriate opportunity is older than 6 months old, the Channel Manager should deny the registration and work with the partner to see if there is an upcoming licensing opportunity that would make sense for the services.
5. Click Approve/Deny/Return Registration.
   ![7-Approve_Deny_Return](/handbook/sales/field-operations/channel-operations/images/7-Approve_Deny_Return.png)
6. On the next page, choose approve, and if you have a message for the partner about the deal, you can add that into the “comments” section. This is not required, but anything included in this field will be sent back to the partner.
   ![8-Approve_Deny_Details](/handbook/sales/field-operations/channel-operations/images/8-Approve_Deny_Details.png)
   - If a distributor is involved, choose the distributor in the field that appears. If no distributor is involved, this field can be left blank.
7. Both the registration record and the opportunity record will be updated with the approval information.
   - A Service Attach registration will **not** populate the Partner Sourced Deal Registration section of an opportunity. To find the Service Attach Deal Registration, click the related list link at the top of the opportunity. This will bring you to a list of any registration attached to the opportunity, including the Service Attach Deal Registration.
   ![10-Reg_Related_Lists](/handbook/sales/field-operations/channel-operations/images/10-Reg_Related_Lists.png)  
   - Alternatively, you can scroll to the “Registrations” section toward the bottom of the opportunity.
   ![11-Reg_for_Svc_Att](/handbook/sales/field-operations/channel-operations/images/11-Reg_for_Svc_Att.png)  
8. If you have created a new opportunity during this process, chatter @sales-support and request that the opportunity owner be updated to the name of the sales team member who owns the customer account.
 
Rebate payouts will be reported and paid after each GitLab quarter close.
 
### Additional Information
- The resale discount will be administered as an upfront discount from the GitLab license price on the most recent product sale net license price. The Service Attach incentive will be paid out at the end of each GitLab fiscal quarter.
- Partner Service Attach incentives are as follows:
  - 2-4 Services per quarter: 2.5% on license net ARR (Max payout $2500/closed-won opportunity)
  - 5-9 Services per quarter: 5% on license net ARR (Max payout $5000/closed-won opportunity)
  - 10 or more Services per quarter: 7.5% on license net ARR (Max payout $7500/closed-won opportunity)
- Partners must register a Services Attach deal registration and provide proof of performance to qualify for the incentive.
- Rebates and referral fees may require CRO approval. 
- Discounts are off list price.  If GitLab is deeply discounting a large ARR customer engagement, the partner can reasonably expect to share in that with a discount reduction.  The Partner, GitLab Sales, Channel Account Manager must agree on the negotiated discount amount.
 
For more information on quoting or the Partner Program, please visit:
- [Deal Desk Quote Configuration](https://about.gitlab.com/handbook/sales/field-operations/sales-operations/deal-desk/#zuora-quote-configuration-guide---standard-quotes)
- [Partner Program](https://about.gitlab.com/handbook/resellers/)
- [Channel Discount Matrices for GitLab Team Members](https://about.gitlab.com/handbook/sales/field-operations/channel-operations/#partner-program-discounts).  
 
 
## Partner Help Desk Support and Communication
 
**Internal Team Members:** Chatter @Partner Help Desk in Salesforce or for general questions, post in the Slack channel #channel-programs-ops.  

**External Communication:** Email partnersupport@gitlab.com to include a partner or other external stakeholder for help with partner-related requests. PHD team members monitor the queue and email inbox throughout the day in all time zones.  

Partner Help Desk’s primary internal communication channel is Salesforce Chatter. When you chatter @Partner Help Desk, it will automatically create a case in the Partner Help Desk (PHD) queue.  

**Please do not tag PHD team members directly in chatter or make a request through Slack direct message. Always use @Partner Help Desk for SFDC requests or post in #channel-programs-ops in Slack for general questions.**. This ensures our team is working as efficiently as possible for everyone and that you are covered in case the team member who replied first becomes unavailable.
If someone is working on a case, they will continue to support until the case is closed. If a matter was resolved, but needs to be revisited, please chatter @Partner Help Desk to reopen a case.
To Chatter the PHD team, tag @Partner Help Desk in Chatter on the related opportunity or account page and a short sentence on your request. If the PHD team needs more information or needs to pull in another team, we will follow up directly via Chatter. If you need to involve a partner, please email partnersupport@gitlab.com, instead of an individual PHD team member so any team member can jump in as something moves forward.
 
 
##  Program and Incentive Definitions
 
- The GitLab Partner Program provides partners with set discounts based on their program status and whether or not there is an active deal registration.
- At least one partner employee must complete the Sales Core training for the partner to qualify for deal registration and program discounts.
- GitLab employees can access the discount tables using the links below:
  - [Commercial Matrix](https://gitlab.my.salesforce.com/0694M00000DsShm?retUrl=%2F_ui%2Fcore%2Fchatter%2Ffiles%2FFileTabPage)
  - [Public Sector Matrix](https://gitlab.my.salesforce.com/0694M00000DsShr?retUrl=%2F_ui%2Fcore%2Fchatter%2Ffiles%2FFileTabPage)
  - [MSP Matrix](https://gitlab.my.salesforce.com/0694M00000DsShw?retUrl=%2F_ui%2Fcore%2Fchatter%2Ffiles%2FFileTabPage)

  _These matrices represent the discounts effective August 15, 2021_.  

- Partners can find the discount table in the Asset Library on the [GitLab Partner Portal](https://partners.gitlab.com/).
 
## Partner Applicant Approval / Denial - Granting Portal Access
Partner Program participation sign ups must be initiated by the Partner in the Partner Portal application form which can be found [here](https://rv.treehousei.com/en/login.aspx). In the partner application process, channel partners review the partner contract, including both the resale and referral addenda, review the partner program guide, complete their application form and agree to program terms and conditions. Technology partners are not able to agree to the terms and conditions during the application process.
If an authorized representative of the channel partner accepts the agreement terms, they (or the person entering the application) will need to select “Yes” that they agree to terms on the application form. Once they have agreed, they will automatically be set to “ Authorized” and will get immediate access to the partner portal. At this time, partners will be set up in both Salesforce and the partner portal at Authorized and their track set to Open.
The partner will receive an email confirming the receipt of their application, and applicable Channel Sales or Alliance Manager will receive a New Partner notification email from Partnersupport@gitlab.com notifying there is a new partner applicant in that region. Channel Sales Managers will be notified of each partner application in their regions, whether they agreed to the terms or not.
Upon receiving notification they will be responsible for reviewing the partner’s information and deactivating any inappropriate partners. They will also need to set the Partner Type in Salesforce for newly authorized partners.
For partners that have questions about the contract or need to negotiate terms and conditions, Channel Sales Managers are responsible for working with the partner offline to address questions and come to agreement on program terms. Upon receiving the New Partner Applicant notification email, the applicable Channels Sales Manager needs to complete the following:
1. Contact the partner and qualify them.
2. If the decision is to move forward with the partner first check to see if a partner account already exists in Salesforce. If it is a duplicate, request for the accounts to be merged by the Channel Operations team. If the decision is to deny the partner then go to step #7.
3. To start the contracting process click the Legal Request button in SFDC on the partner account record.
  - Request the appropriate contract addendum (Resale, Referral/Services or both **OR** MSP **OR** OTHER). Default should be Resale and Referral/Services.
4. Once the contract is fully executed and attached to the partner account record in SFDC the following fields need to be updated by the Channel Sales Manager and are required(*) in order to save the account.
  - *Change Partner Status = Authorized.
  - *Select Partner Type.
  - For partners that signed standard contract terms, set Partner Program Status to “New”.
  - Please update the partner record to be as complete as possible.
  - For additional information on the Partner Program review [here](https://about.gitlab.com/handbook/resellers/#partner-program-tracks)
5. Once a partner is authorized, each SFDC contact for that partner will automatically receive a message with login credentials to the portal.
6. Additional partner employees can go to partners.gitlab.com to register. Once they are linked to an authorized partner account (they must select the correct account upon registering), they will automatically receive a message with login credentials. If the account is still a Prospect they will not have access until the account has an executed contract and is moved to Authorized.
7. If the decision is to not move forward with the partner, the Channel Sales Manager needs to set Partner Status = Denied. 
Technology partners use the same form, but are not able to agree to the terms and conditions. Once they submit the form, they will be set to active. If the Alliances team wants to establish a contract with the partner, they must follow the Legal Request process in Salesforce. 
If for any reason, a partner account needs to be created in Salesforce directly, requests for account creation can be made to #channel-programs-ops within Slack.
Visit the [Partner Applicant/Partner Portal FAQ](https://docs.google.com/document/d/1aPCqF5-qb2XxFEhvkNzvexwsIYGuiJF8AhK_qeUgw0Y/edit?usp=sharing) for additional information.
 
 
 
 
## Channel Partner Price Files
GitLab will provide Channel Price Files for distributors and direct resellers approximately 30 days before intended changes. **Only Channel Managers should be sharing Channel Price Files.**
The following price files are provided by Channel Ops:
- Distribution Price Files for Resale Opportunities, including reseller and distributor discounts for the main program.
- Public Sector Price Files for Resale Opportunities, including reseller and distributor discounts for the main program.
- Partner (Direct Reseller) Price Files for Resale Opportunities, including reseller discounts for the main program.
- List Price File with no discounts.
 
 
### Locating and Sharing Channel Price Files
 
Price Files can be found [in this folder](https://drive.google.com/drive/folders/1UCNH77wTF4eCiCeAQHqDItkCGITl534D?usp=sharing).
 
When sharing a Channel Price File with a partner (either a distributor or reseller), please do NOT share the folder or file location. To share a document, please either copy it into your own google drive and update the permissions accordingly when you share a link, or attach a downloaded copy to an email to a partner. No partners should be given access to this folder. Only Channel Managers should be sharing Channel Price Files.
 
### Naming Conventions and Which File to Use
 
Within the Price List Folder, there are other folders. For the current active price file, always use the one with the most recent date that has not passed yet. The folder name will also say "ACTIVE" at the front of it.
 
![12-Price_File_Folders](/handbook/sales/field-operations/channel-operations/images/12-Price_File_Folders.png)
 
![13-Price_File_List](/handbook/sales/field-operations/channel-operations/images/13-Price_File_List.png)
 
If there are any questions, please reach out to the **#channel-programs-ops** Slack channel.
 
 
## Alliances and OEMs
Please visit the [Alliances Handbook](https://about.gitlab.com/handbook/alliances/) for an overview of the GitLab Alliance Team. If you are a GitLab employee, the [Private Alliance Handbook](https://gitlab-com.gitlab.io/alliances/alliances-internal/) is another available resource. The [Alliances Salesforce Dashboard](https://gitlab.my.salesforce.com/01Z4M000000oYAp) is also available.
For any questions regarding our Alliance partners, please reach out to the #alliances Slack channel. If your inquiry is deal-specific, please use one of these Slack channels: **#a_gcp_deal_registration, #a_aws_deal_registration, #a_ibm_deal_registration**.
 
### Opportunity Tagging for GCP and AWS Deals
***Use Case 1: Partner Co-Sell (Marketplace transaction with no source credit)*** 

If a deal is being transacted through **GCP Marketplace** or **AWS Marketplace**, then the following fields need to be filled out on the opportunity:
- **Partner Co-Sell** = [GCP](https://gitlab.my.salesforce.com/0014M00001nJhks)* or [AWS](https://gitlab.my.salesforce.com/0014M00001ldTdt)*  
_*Be sure to use the correct SFDC account. Click on the link to confirm the GCP and/or AWS account.)_  
 
***Use Case 2: Partner Sourced Deal Registration (No Marketplace transaction)***  

If GCP or AWS brought us a lead/referred GitLab a deal, but will not be transacting on Marketplace, then the following fields should be filled out on the opportunity:
- **DR - Partner** should be filled out using [GCP](https://gitlab.my.salesforce.com/0014M00001nJhks) or [AWS](https://gitlab.my.salesforce.com/0014M00001ldTdt) account
- **DR - Partner Deal Type** = Referral 
 
***Use Case 3: Partner Sourced Deal Registration (Marketplace transaction)***  
If GCP or AWS brought us a lead/referred GitLab a deal, and will be transacting on Marketplace, then the following fields should be filled out on the opportunity:
- **DR - Partner** should be filled out using [GCP](https://gitlab.my.salesforce.com/0014M00001nJhks) or [AWS](https://gitlab.my.salesforce.com/0014M00001ldTdt) account
- **DR - Partner Deal Type** = Resale 
 
***Use Case 4: Partner Influence (No Marketplace transaction and no source credit)***

If GCP or AWS support a deal and help drive the customer to buy GitLab, but were not the original source of the opportunity nor are they transacting the deal, then the following field should be filled out on the Opportunity:
- **Influence Partner** should be filled out using [GCP](https://gitlab.my.salesforce.com/0014M00001nJhks) or [AWS](https://gitlab.my.salesforce.com/0014M00001ldTdt) account
 
### Requesting Google Cloud Credits
Required fields when requesting Google Cloud Credits on an Opportunity
1. Have you engaged with the GCP Team already? (Drop down: Yes, No)
2. Customer open to being a reference? (drop down: logo use, case-study, joint speaking session, etc.)
3. Credits being requested (Sales Rep enters in the amount of credits required to close the deal)
Once all required information is submitted, the request  will be routed internally for approval. If approved, the opportunity is updated to ‘approved’ and the opportunity owner  is notified via email. GCP Credits are provided directly to the customer by the GCP Team after acceptance of the private offer. For more information, visit the [Private Alliance Handbook](https://gitlab-com.gitlab.io/alliances/alliances-internal/gcp/).
 
### IBM Partner Requests & QTC Process
1. IBM Partner Requests: Visit the [Private Alliance Handbook](https://gitlab-com.gitlab.io/alliances/alliances-internal/ibm/)
2. Common Use Cases:
  - IBM Partner Request
  - IBM/GitLab Identify Opportunity
  - IBM Partner Order
Additional Information on the QTC Process is available in the [Private Alliance Handbook](https://gitlab-com.gitlab.io/alliances/alliances-internal/ibm/).
 
## Compensation on Channel Opportunities
 
### Channel Compensation Policy for Cross-Geo Partner Deals
 
**Cross-Geo _Partner_ Deals** are defined as opportunities that land (ship to) a GEO that differs from the Partner's GEO headquarters (i.e. ‘Landing GEO’ vs. ‘HQ GEO’).
GitLab must support Cross-GEO Deals in order to strengthen and enforce the channel business behaviors: collaboration, results and efficiency. The primary focus of Channel Managers is building revenue in their assigned territory and GEO. Therefore, managers should engage their peers on Cross-GEO Deals to ensure success for both GitLab and the Partner.
In the event of a Cross-GEO Deal, the Channel Manager located in the ‘Landing GEO’ is the Channel Manager of record and receives compensation on the opportunity. The expectation is that the ‘Landing GEO’ Channel Manager takes lead on the opportunity and works with the partner and customer to drive the deal.
To ensure correct compensation, the applicable Channel Manager needs to populate their name in the Channel Manager field in SFDC on the Opportunity.
 
### Channel Neutral
Comp Neutrality applies to all GitLab Opportunities where a Partner is involved in the physical transaction (takes paper) ***and*** a Partner Program discount is applied on the executed Partner Order Form.
The maximum Comp Neutral payout is based on the applicable GitLab Partner Program discount matrix. If additional discount is needed in order to close the deal, that portion is not eligible for Comp Neutrality.
In the event a lesser discount is applied on the deal then what the Partner Program allocates, then the lesser discount will also be what is calculated for Comp Neutrality.
Other factors:
- Starter/Bronze is not applicable even if a discount is approved
- US PubSec Sales, Channel, and Alliances do not qualify
- For Alliances, the contractual discount is used to calculate comp neutral
- IBM OEM deals do not qualify
- For H1 FY22 comp neutral did not apply for Distribution discounts. Starting H2 FY22 the contractual Distribution discounts will apply however no retroactive payments/re-calculations will be made.
As a reminder, comp neutrality only applies to comp and does not retire quota. For additional information, please review [Channel Neutral Compensation](https://about.gitlab.com/handbook/sales/commissions/#channel-neutral-compensation) within the Sales Ops commission section of the Handbook.
Internal: [Partner Program Discount Matrix](https://about.gitlab.com/handbook/sales/field-operations/channel-operations/#program-and-incentive-definitions)
For Partner Program discounts specific to US PubSec, please reach out to #channel-ops on Slack.
 
### 2-Tier Opportunities
If the Opportunity is going through a 2-Tier Channel (Distributor + Reseller), both Partner Accounts must be included on the Opportunity in the Partner Information section in order for both Program discounts to apply AND for Comp Neutral to calculate on both. Reseller should be in the Partner  field and the applicable Distributor in the Distributor field on the Opportunity.
 
### Opportunities with GCP and AWS
For deals going through GCP and AWS, the Partner fields should still be filled out on the Opportunity but comp neutral will not calculate until the deal closes as partner discounts are done after the quote/order form is generated. Deal Desk will assist with this process.
 
 


